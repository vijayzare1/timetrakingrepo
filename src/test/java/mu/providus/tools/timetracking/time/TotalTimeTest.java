package mu.providus.tools.timetracking.time;

import mu.providus.tools.timetracking.helper.SummaryHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TotalTimeTest {
    @Test
    public void testValidNormalFlow() {
        String[] in = {"10/19/2020 8:00:00 AM", "10/19/2020 10:00:00 AM", "10/19/2020 2:00:00 PM"};
        String[] out = {"10/19/2020 9:00:00 AM", "10/19/2020 1:30:00 PM", "10/19/2020 5:00:00 PM"};

        float total = new SummaryHelper().calculateHours(in, out);

        Assertions.assertEquals(7.5, total);
    }

    @Test
    public void testInvalidNormalFlow() {
        String[] in = {"10/19/2020 8:00:00 AM", "10/19/2020 10:00:00 AM", "10/19/2020 2:00:00 PM"};
        String[] out = {"10/19/2020 9:00:00 AM", "10/19/2020 5:00:00 PM"};

        Assertions.assertThrows(Exception.class, () -> {
            float total = new SummaryHelper().calculateHours(in, out);
            Assertions.assertEquals(7.5, total);
        });
    }

    @Test
    public void testValidIsNormalFlow() {
        String[] in = {"10/19/2020 8:00:00 AM", "10/19/2020 10:00:00 AM", "10/19/2020 2:00:00 PM"};
        String[] out = {"10/19/2020 9:00:00 AM", "10/19/2020 1:30:00 PM", "10/19/2020 5:00:00 PM"};

        boolean actual = new SummaryHelper().isNormalFlow(in, out);

        Assertions.assertTrue(actual);
    }

    @Test
    public void testIsNormalFlowDifferentSize() {
        String[] in = {"10/19/2020 8:00:00 AM", "10/19/2020 10:00:00 AM", "10/19/2020 2:00:00 PM"};
        String[] out = {"10/19/2020 9:00:00 AM", "10/19/2020 5:00:00 PM"};
        boolean actual = new SummaryHelper().isNormalFlow(in, out);
        Assertions.assertFalse(actual);
    }

    @Test
    public void testIsNormalFlowMissingIn() {
        String[] in = {"10/19/2020 8:00:00 AM", "10/19/2020 11:00:00 AM", "10/19/2020 2:00:00 PM"};
        String[] out = {"10/19/2020 9:00:00 AM", "10/19/2020 10:00:00 AM",  "10/19/2020 5:00:00 PM"};
        boolean actual = new SummaryHelper().isNormalFlow(in, out);
        Assertions.assertFalse(actual);
    }




}
