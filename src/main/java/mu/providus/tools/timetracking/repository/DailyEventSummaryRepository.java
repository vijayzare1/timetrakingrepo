package mu.providus.tools.timetracking.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.format.annotation.DateTimeFormat;

import mu.providus.tools.timetracking.model.DailyEventSummary;

public interface DailyEventSummaryRepository extends JpaRepository<DailyEventSummary, Integer> {
	// List<DailyEventSummary> findByDayContaining(@RequestParam("des_day")
	// LocalDate des_day);
//	List<DailyEventSummary> findAllByDay(Date day);
//
//	List<DailyEventSummary> findByDay(LocalDate day);

	List<DailyEventSummary> findByDayContaining(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate day);

	List<DailyEventSummary> findByDay(LocalDate day);

	List<DailyEventSummary> findByUserContaining(String user);

	List<DailyEventSummary> findByDateContaining(String date);

	
}
