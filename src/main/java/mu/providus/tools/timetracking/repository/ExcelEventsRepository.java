package mu.providus.tools.timetracking.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mu.providus.tools.timetracking.model.EmployeeEvents;

public interface ExcelEventsRepository extends JpaRepository<EmployeeEvents, Long> {



}
