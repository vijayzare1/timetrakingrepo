package mu.providus.tools.timetracking.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.Data;

@Entity
@Table(name = "daily_event_summary", schema = "qlms")
@Data
public class DailyEventSummary {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "des_summary_id")
	private Integer summaryId;

	// @Column(name = "des_day")

//	@Temporal(TemporalType.DATE)
	private LocalDate day;

	@JsonFormat(pattern = "yyyy-MM-dd", shape = Shape.STRING)
	private String date;

	@Column(name = "des_duration")
	private Integer duration;

	@Column(name = "des_card_no")
	private String cardNo;

	@Column(name = "des_user")
	private String user;

	@Column(name = "des_employee_event_id")
	private Integer employeeEventId;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "events", referencedColumnName = "des_summary_id")
	private List<EmployeeEvents> events = new ArrayList<>();

	public DailyEventSummary() {

	}

	public DailyEventSummary(LocalDate day, String date, Integer duration, String cardNo, String user,
			Integer employeeEventId, List<EmployeeEvents> events) {
		super();
		this.day = day;
		this.date = date;
		this.duration = duration;
		this.cardNo = cardNo;
		this.user = user;
		this.employeeEventId = employeeEventId;
		this.events = events;
	}

}
