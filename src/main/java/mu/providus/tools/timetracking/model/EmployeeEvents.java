	package mu.providus.tools.timetracking.model;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "employee_events", schema = "qlms")

public class EmployeeEvents {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ee_employee_eventid")
    private Integer employeeEventId;

    @Column(name = "ee_directionid")
    private Integer directionId;

    @Column(name = "ee_eventtime")
    private LocalDateTime eventTime;

    @Column(name = "ee_cardno")
    private String cardNo;

    @Column(name = "ee_user")
    private String user;

    public EmployeeEvents() {

    }

    public EmployeeEvents(Integer directionId, LocalDateTime eventTime, String cardNo, String user) {
        this.directionId = directionId;
        this.eventTime = eventTime;
        this.cardNo = cardNo;
        this.user = user;
    }

    public Integer getEmployeeEventId() {
        return employeeEventId;
    }

    public void setEmployeeEventId(Integer employeeEventId) {
        this.employeeEventId = employeeEventId;
    }

    public Integer getDirectionId() {
        return directionId;
    }

    public void setDirectionId(Integer directionId) {
        this.directionId = directionId;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }


    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "EmployeeEvents{" +
                "employeeEventId=" + employeeEventId +
                ", directionId=" + directionId +
                ", eventTime='" + eventTime + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
