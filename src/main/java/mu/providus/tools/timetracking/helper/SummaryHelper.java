package mu.providus.tools.timetracking.helper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class SummaryHelper {

    public boolean isNormalFlow(String[] in, String out[]) {
        return isNormalFlow(toLocalDatetime(in), toLocalDatetime(out));
    }

    public boolean isNormalFlow(List<LocalDateTime> inLdt, List<LocalDateTime> outDt) {

        if (inLdt.size() != outDt.size()) {
            return false;
        }

        for (int i = 0; i < inLdt.size(); i++) {
            if (outDt.get(i).isBefore(inLdt.get(i))) {
                return false;
            }
        }

        return true;
    }


    public float calculateHours(String[] in, String out[]) {

        List<LocalDateTime> inLdt = toLocalDatetime(in);
        List<LocalDateTime> outDt = toLocalDatetime(out);

        return calculateHours(inLdt, outDt);
    }

    private List<LocalDateTime> toLocalDatetime(String[] in) {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("MM/dd/yyyy h:m:ss a").toFormatter(Locale.ENGLISH);
        return Arrays.stream(in).map(t -> LocalDateTime.parse(t, formatter)).collect(Collectors.toList());
    }

    public float calculateHours(List<LocalDateTime> inLdt, List<LocalDateTime> outDt) {
        float total = 0.0f;
        for (int i = 0; i < inLdt.size(); i++) {
            total += inLdt.get(i).until(outDt.get(i), ChronoUnit.MINUTES);
        }
        return total / 60;
    }
}











