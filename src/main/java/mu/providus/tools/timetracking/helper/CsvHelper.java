package mu.providus.tools.timetracking.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.nio.charset.StandardCharsets;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import mu.providus.tools.timetracking.model.EmployeeEvents;

public class CsvHelper {
    public static String TYPE = "text/csv";


    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<EmployeeEvents> csvToEvents(InputStream is, int direction) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_16));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withTrim())) {
            HashSet<EmployeeEvents> hashSet = new HashSet<>();

            List<EmployeeEvents> events = new ArrayList<EmployeeEvents>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                String eventTime = csvRecord.get(2).replaceAll("\"", "");
                String cardNo = csvRecord.get(3).replaceAll("\"", "");
                String user = csvRecord.get(4).replaceAll("\"", "");
                DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("MM/dd/yyyy h:m:ss a")
                        .toFormatter(Locale.ENGLISH);
                EmployeeEvents event = new EmployeeEvents(direction, LocalDateTime.parse(eventTime, formatter), cardNo, user);

                events.add(event);
            }

            return events;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }

}
