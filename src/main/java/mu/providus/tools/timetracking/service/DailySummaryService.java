package mu.providus.tools.timetracking.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mu.providus.tools.timetracking.model.DailyEventSummary;
import mu.providus.tools.timetracking.model.EmployeeEvents;
import mu.providus.tools.timetracking.repository.EmployeeEventsRepository;


@Service
@Transactional
public class DailySummaryService {
    private EmployeeEventsRepository employeeEventsRepository;

    public List<EmployeeEvents> allEvents() {
        List<DailyEventSummary> allSummary = new ArrayList<DailyEventSummary>();
        List<EmployeeEvents> allEmployeeData = employeeEventsRepository.findAll();

        DailyEventSummary dailyEventSummary;
        if (allEmployeeData != null) {
            allSummary.forEach(dailyEvent -> {
                EmployeeEvents employeeEvents = new EmployeeEvents();
                dailyEvent.setCardNo(employeeEvents.getCardNo());
                dailyEvent.setUser(employeeEvents.getUser());
                dailyEvent.setEmployeeEventId(employeeEvents.getEmployeeEventId());
//                dailyEvent.setDirectionId(employeeEvents.getDirectionId());
//                dailyEvent.setEventTime(employeeEvents.getEventTime());


                allSummary.add(dailyEvent);
            });


        } else {
            return allEmployeeData;


    }
        return allEvents();
}
}




