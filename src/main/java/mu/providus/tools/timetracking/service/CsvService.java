package mu.providus.tools.timetracking.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mu.providus.tools.timetracking.helper.CsvHelper;
import mu.providus.tools.timetracking.model.EmployeeEvents;
import mu.providus.tools.timetracking.repository.ExcelEventsRepository;

@Service
public class CsvService {
    @Autowired
    ExcelEventsRepository repository;

    public void save(MultipartFile file, int direction) {

        try {
            List<EmployeeEvents> events = CsvHelper.csvToEvents(file.getInputStream(), direction);
            repository.saveAll(events);
        }
         catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    public List<EmployeeEvents> getAllEvents() {
        return repository.findAll();
    }



}
