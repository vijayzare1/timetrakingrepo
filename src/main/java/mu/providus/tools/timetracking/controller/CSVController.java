package mu.providus.tools.timetracking.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import mu.providus.tools.timetracking.service.CsvService;
import mu.providus.tools.timetracking.helper.CsvHelper;
import mu.providus.tools.timetracking.message.ResponseMessage;


@Controller
@RequestMapping("/api/csv")
public class CSVController {
    @Autowired
    CsvService fileService;


    @PostMapping("/upload/{direction}")
    public ResponseEntity<ResponseMessage> uploadFile(@PathVariable("direction") String direction, @RequestParam("file") MultipartFile file) {
        String message = "";
        System.out.println(">>>>>>>>>>. UPLOAD");

        if (CsvHelper.hasCSVFormat(file)) {
            try {
                int directionInt;
                if (direction.equals("in")) {
                    directionInt = 1;
                } else if (direction.equals("out")) {
                    directionInt = 2;
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage("invalid direction"));
                }

                fileService.save(file, directionInt);


                message = "Uploaded the file successfully: " + file.getOriginalFilename() + direction;

                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

            } catch (Exception e) {
                LoggerFactory.getLogger(CSVController.class).error("upload csv error", e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));

    }
}

