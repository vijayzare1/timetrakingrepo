package mu.providus.tools.timetracking.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mu.providus.tools.timetracking.dao.EventDao;
import mu.providus.tools.timetracking.dao.SummeryDao;
import mu.providus.tools.timetracking.exception.NotFoundException;
import mu.providus.tools.timetracking.model.DailyEventSummary;
import mu.providus.tools.timetracking.model.EmployeeEvents;
import mu.providus.tools.timetracking.repository.DailyEventSummaryRepository;
import mu.providus.tools.timetracking.repository.EmployeeEventsRepository;

@RestController
@RequestMapping("/api/events")
public class DailySummaryController {

	@Autowired
	DailyEventSummaryRepository summaryRepository;
	@Autowired
	EmployeeEventsRepository eventRepository;

	@GetMapping("/get-all-employees")
	public List<DailyEventSummary> getAllsummary() {

		return summaryRepository.findAll();
	}

	@GetMapping("/dates/day")
	public List<DailyEventSummary> getEvetByDate(@RequestParam("day") LocalDate day) {

		return summaryRepository.findByDayContaining(day);
	}

	@GetMapping("/dates/{date}")
	public List<DailyEventSummary> findAllByDate(@PathVariable String date) {

		return summaryRepository.findByDateContaining(date);
	}

	@PostMapping("/date")
	private String createSmmery(@RequestBody SummeryDao dao) {

		DailyEventSummary summery = new DailyEventSummary();
		summery.setCardNo(dao.getCardNo());
		summery.setUser(dao.getUser());
		summery.setDate("1994-10-13");
		summery.setDuration(dao.getDuration());
		summery.setEmployeeEventId(dao.getEmployeeEventId());
		summaryRepository.save(summery);
		return "addded Succes";
	}
//	public List<DailyEventSummary>findByDate(String datepart){
//	
//		LocalDate localDatePart =  SupportUtils.convertStringDateToLocalDate(datepart);
//		
//		return summaryRepository.findByDay(localDatePart).stream().map(DailyEventSummaryMapper::toDto).
//	}

	@PostMapping("/pEvent/{employeeEventId}")
	public DailyEventSummary createEvent(@PathVariable Integer des_summary_id, @RequestBody EventDao dao) {

		EmployeeEvents events = new EmployeeEvents();

		if (!summaryRepository.existsById(des_summary_id)) {
			System.out.println(des_summary_id + "not found");
			throw new NotFoundException(des_summary_id + "This event is empty");
		}

		return summaryRepository.findById(des_summary_id).map(summery -> {
			// summery.setDay(LocalDate.now());
			events.setCardNo(dao.getCardNo());
			events.setDirectionId(dao.getDirectionId());
			events.setEventTime(LocalDateTime.now());
			events.setUser(dao.getUser());
			summery.getEvents().add(events);

			return summaryRepository.save(summery);
		}).orElseThrow(() -> new NotFoundException("hare event insertation faild"));
	}

	@GetMapping("/get-event/{id}")
	public Optional<DailyEventSummary> getsummarybyId(@PathVariable(value = "id") Integer summaryId) {

		return summaryRepository.findById(summaryId);
	}

	@PostMapping("/generate")
	public ResponseEntity createEvent(@RequestParam(name = "date", required = false) String date) {

		return ResponseEntity.ok().body(true);
	}

}
