package mu.providus.tools.timetracking.dao;

import java.time.LocalDate;

import lombok.Data;

@Data
public class SummeryDao {

	private LocalDate day;

	private Integer duration;

	private String cardNo;

	private String user;

	private Integer employeeEventId;

	private String date;

}
